package com.example.sismi.monicaproto2;

import android.location.Location;
import android.util.Log;

public class LaunchConnection implements Runnable {

	private Client c;
	private boolean isRunning = true;


	@Override
	public void run() {

		//Création d'un objet qui va contenir les connections
		c = new Client("192.168.1.42",9999);

		while(isRunning){

			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Log.d("END","Fin du LT");

	}

	public void sendTxt(String str) throws Exception {

		if(c==null){
			throw new Exception("Pas de connection");
		}

		c.sendTxt(str);
	}

	public void close(){
		if(c!=null){
			this.c.close();
		}

		this.isRunning = false;
	}

    public void sendPos(Location location) throws Exception {
        if(c==null){
            throw new Exception("Pas de connection");
        }

        c.sendPos(location);
    }
}
