package com.example.sismi.monicaproto2;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

public class PredefinedCatView implements PredefinedViewCat{

    private int viewSelected=0;
    private Context context;
    private ScrollView scrollView;
    private LinearLayout linearLayout;
    private static String predText1;
    private static String predText2;


    private ArrayList<View> views = new ArrayList<>();

    public PredefinedCatView(Context context, LinearLayout linearLayout, ScrollView scrollView) {
        this.context = context;

        this.linearLayout = linearLayout;
        this.scrollView = scrollView;

        predText1 = context.getString(R.string.Cat1);
        predText2 = context.getString(R.string.Cat2);

        LayoutInflater layoutInflater = LayoutInflater.from(context);

        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText1);
        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText2);

        views.get(this.viewSelected).setBackgroundColor(ContextCompat.getColor(context, R.color.colorSelectTextColor)); //Premier selectionné

    }

    public void addViewsTo(){
        for(View view : views){
            linearLayout.addView(view);
        }
    }

    public void removeViewsTo(){
        for(View view : views){
            linearLayout.removeView(view);
        }
    }

    @Override
    public void goDown() {
        if(viewSelected<views.size()-1 ){
            views.get(viewSelected).setBackgroundColor(ContextCompat.getColor(this.context, R.color.colorUnselectedTextBorder)); //previous one
            viewSelected ++;
            views.get(viewSelected).setBackgroundColor(ContextCompat.getColor(this.context, R.color.colorSelectTextColor)); // New one
        }
        scrollView.smoothScrollTo(0,views.get(viewSelected).getTop()- scrollView.getHeight()/2 + views.get(viewSelected).getHeight()/2);
    }

    @Override
    public void goUp() {
        if(viewSelected>0){
            views.get(viewSelected).setBackgroundColor(ContextCompat.getColor(this.context, R.color.colorUnselectedTextBorder)); //previous one
            viewSelected --;
            views.get(viewSelected).setBackgroundColor(ContextCompat.getColor(this.context, R.color.colorSelectTextColor));// New one
            scrollView.smoothScrollTo(0,views.get(viewSelected).getTop()- scrollView.getHeight()/2 + views.get(viewSelected).getHeight()/2);
        }

    }

    @Override
    public Integer getSelectedMessage() {
       return viewSelected;
    }
}
