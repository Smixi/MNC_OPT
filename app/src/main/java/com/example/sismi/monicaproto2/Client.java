package com.example.sismi.monicaproto2;

import android.location.Location;
import android.util.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;

public class Client {
	
	private int clientPort;
	Socket clientToServer;
	ClientThreadClientToServer serverT;
	ClientThreadServerToClient clientT;
	ProtocolClientClientSide prot;
	ServerSocket server;
	private int serverPort = 9999;
	private Socket clientS;
	
	public Client(String host, int port) {
		try {

			clientToServer = new Socket(host,port); //On se connecte au serveur
			//On cherche l'ip local en Wifi

			Log.d("Local_IP: ",clientToServer.getLocalAddress().toString());
			this.server = new ServerSocket(this.serverPort, 100, clientToServer.getLocalAddress());

			Log.d("ServerSocket",server.toString());
			this.clientS = server.accept(); //On garde la socket du serveur distant qui joue le role du client

			serverT = new ClientThreadClientToServer(clientS);
			clientT = new ClientThreadServerToClient(clientToServer);
			
			//On lance les deux thread
			new Thread(clientT).start();
			new Thread(serverT).start();
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void sendTxt(String txt) {
		this.clientT.doCmd(ProtocolConstants.SEND_TXT,txt);
	}

	public void close(){
		if(this.clientT!=null){
			this.clientT.close();
		}
		if(this.server != null){
			this.serverT.close();
		}


		try {
			if(this.server!=null){
				this.server.close();
			}
			if(this.clientS!=null){
				this.clientS.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    public void sendPos(Location location) {
	    this.clientT.doCmd(ProtocolConstants.SEND_POS,location);
    }
}
