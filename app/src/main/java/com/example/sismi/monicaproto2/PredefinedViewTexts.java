package com.example.sismi.monicaproto2;

import android.graphics.drawable.Drawable;
import android.view.View;

public interface PredefinedViewTexts {
    abstract void addViewsTo();
    abstract void goDown();
    abstract void goUp();
    abstract void removeViewsTo();
    abstract Object getSelectedMessage();
    abstract View getSelectedView();
    public Drawable getPredImg();
}
