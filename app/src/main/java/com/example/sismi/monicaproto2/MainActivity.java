package com.example.sismi.monicaproto2;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Environment;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    private static MainActivity instance;
    private static ClientHandler mainActivityHandler;

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static Handler getMainActivityHandler() {
        return mainActivityHandler;
    }

    private LaunchConnection lt = null;

    private ConstraintLayout constraintLayout;

    enum UIViewState {
        VOID, CHOOSING_MENU, CHOOSING_CAT, CHOOSING_PREDEFINED_MSG, CONFIRM_SEND;
    }

    ;

    UIViewState currentState;

    ArrayList<PredefinedViewTexts> predefinedTexts = new ArrayList<>();
    PredefinedCatView predefinedCatView;
    ScrollView scrollView;
    LinearLayout linearLayout;
    LocationManager locationManage;
    Sensor s = new Sensor();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.predefined_view);
        this.scrollView = findViewById(R.id.scroll);
        linearLayout = this.scrollView.findViewById(R.id.linear);


        predefinedTexts.add(new PredefinedTextsViewCat1(this, linearLayout, scrollView)); //On rajoute les messages cat1
        predefinedTexts.add(new PredefinedTextsViewCat2(this, linearLayout, scrollView)); //On rajoute les messages cat 2

        currentState = UIViewState.CHOOSING_CAT;


        this.predefinedCatView = new PredefinedCatView(this, linearLayout, scrollView);
        this.predefinedCatView.addViewsTo();
        instance = this; //On met a jour le context;
        hideSystemUI(this);

        constraintLayout = findViewById(R.id.cstr);
        locationManage = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Closing", "Closing");
        lt.close();
        lt = null;

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("RESUME", "On relance !");

        mainActivityHandler = new ClientHandler();
        //mainActivityHandler.setServerMessageView((TextView)findViewById(R.id.textView));
        //mainActivityHandler.setImageView((ImageView)findViewById(R.id.imageView));

        //this.input = ((EditText)findViewById(R.id.editText));
        //this.button = (Button) findViewById(R.id.button);

        /*button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    lt.sendTxt(input.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace(); //TODO:Si pas de connection.
                }
            }
        });*/
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.d("FAIL","Pas de permissions");
            return;
        }
        locationManage.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 0, s);
        lt = new LaunchConnection(); //Il faudra l'initialiser et s'assurer que tout est lancé avant de poursuivre.
        Thread t = new Thread(lt);
        t.start();
    }


    public static File getNewImageFile(){
        //May check directory existence before
        File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()+"/opt_"+(System.currentTimeMillis()));
        return f;
    }
    public static File getNewAudioFile(){
        //May check directory existence before
        File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).toString()+"/opt_"+(System.currentTimeMillis()));
        return f;
    }
    public static File getNewVideoFile(){
        //May check directory existence before
        File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).toString()+"/opt_"+(System.currentTimeMillis()));
        return f;
    }

    public static File getHistoryFile(){
        File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString()+"/opt_history.txt");
        if(!f.exists()){
            try {
                f.createNewFile();
            } catch (IOException e) {
                Log.e("File","CANT CREATE THE HISTORY FILE");
                e.printStackTrace();
            }
        }
        return f;
    }

    public static void hideSystemUI(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        //| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        //| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    public static void showSystemUI(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d("KEY",""+keyCode);

        if(keyCode == 25 ){
            switch(currentState){
                case CHOOSING_PREDEFINED_MSG:
                    this.predefinedTexts.get(this.predefinedCatView.getSelectedMessage()).goDown();
                    break;
                case CHOOSING_CAT:
                    this.predefinedCatView.goDown();
                    break;
            }

        }

        if (keyCode == 24){
            switch (currentState){
                case CHOOSING_PREDEFINED_MSG:
                    this.predefinedTexts.get(this.predefinedCatView.getSelectedMessage()).goUp();
                    break;
                case CHOOSING_CAT:
                    this.predefinedCatView.goUp();
                    break;
            }
        }

        if(keyCode == 87){
            switch (currentState){
                case CHOOSING_CAT:
                    this.currentState = UIViewState.CHOOSING_PREDEFINED_MSG;
                    this.predefinedCatView.removeViewsTo();
                    this.predefinedTexts.get(this.predefinedCatView.getSelectedMessage()).addViewsTo();
                    break;
                case CHOOSING_PREDEFINED_MSG:
                    Log.d("SEND", (String)predefinedTexts.get(predefinedCatView.getSelectedMessage()).getSelectedMessage());
                    this.currentState = UIViewState.CONFIRM_SEND;
                    this.predefinedTexts.get(predefinedCatView.getSelectedMessage()).removeViewsTo();

                    this.setContentView(R.layout.confirm_send);
                    ((TextView)this.findViewById(R.id.confirmTxt)).setText((String)predefinedTexts.get(predefinedCatView.getSelectedMessage()).getSelectedMessage());
                    ((ImageView)this.findViewById(R.id.confirmImg)).setImageDrawable(predefinedTexts.get(predefinedCatView.getSelectedMessage()).getPredImg());
                    break;
                case CONFIRM_SEND:
                    this.setContentView(this.constraintLayout);
                    this.predefinedCatView.addViewsTo();
                    this.currentState = UIViewState.CHOOSING_CAT;
                    try {
                        this.lt.sendTxt((String)predefinedTexts.get(predefinedCatView.getSelectedMessage()).getSelectedMessage());


                        View toastView = getLayoutInflater().inflate(R.layout.toast_msg_sent,null);
                        Toast toast = Toast.makeText(this,"Message Sent !",Toast.LENGTH_LONG);
                        toast.setView(toastView);
                        toast.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
        if(keyCode == 88){
            switch (currentState){
                case CHOOSING_PREDEFINED_MSG:
                    this.currentState = UIViewState.CHOOSING_CAT;
                    this.predefinedTexts.get(this.predefinedCatView.getSelectedMessage()).removeViewsTo();
                    this.predefinedCatView.addViewsTo();
                    break;
                case CONFIRM_SEND:
                    this.setContentView(this.constraintLayout);
                    this.predefinedTexts.get(predefinedCatView.getSelectedMessage()).addViewsTo();
                    this.currentState = UIViewState.CHOOSING_PREDEFINED_MSG;
                    break;
            }

        }

        return true;
    }

    class Sensor implements LocationListener {


        @Override
        public void onLocationChanged(Location location) {
            Log.d("TAG",location.toString());
            try {
                lt.sendPos(location);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }


}