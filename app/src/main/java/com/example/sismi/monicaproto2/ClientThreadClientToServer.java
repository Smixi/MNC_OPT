package com.example.sismi.monicaproto2;

import java.io.IOException;
import java.net.Socket;

public class ClientThreadClientToServer implements Runnable {

	private Socket clientToServer;
	private ProtocolServer prot; // TODO: Instantiate this.
	private boolean isRunning = true;
	
	public ClientThreadClientToServer(Socket clientToServer) {
		this.clientToServer = clientToServer;
		this.prot = new ProtocolServerClientSide(clientToServer);
	}
	
	@Override
	public void run() {
		
		System.out.println("Client pret a �couter: "+clientToServer.toString() );
		while(isRunning) {
			try {
				prot.answerTo();
			} catch (IOException e) {
				System.out.println("Erreur de connection ou connection perdue. Fermeture de la socket: ");
				e.printStackTrace();
				try {
					this.clientToServer.close();
					this.close();
				} catch (IOException e1) {
					System.out.println("Impossible de fermer la socket");
					this.close();
					e1.printStackTrace();
				}
			}
		}
		try {
			clientToServer.close();
		} catch (IOException e) {
			System.out.println("Impossible de fermer la socket");
			e.printStackTrace();
		}
	}

	public void close() {
		this.isRunning = false;
	}
	
}
