package com.example.sismi.monicaproto2;

import java.io.IOException;

public interface ProtocolServer {
	public abstract void answerTo() throws IOException;
}
