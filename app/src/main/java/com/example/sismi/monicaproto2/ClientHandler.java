package com.example.sismi.monicaproto2;

import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ClientHandler extends Handler {

    TextView serverMessage = null;
    ImageView imageView = null;
    LaunchConnection lc;

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        ProtocolConstants cmd = ProtocolConstants.values()[msg.what]; //Conversion
        switch (cmd){
            case SEND_TXT:
                this.serverMessage.setText((String) msg.obj);
                break;
            case CLOSE:
                this.lc.close();
                break;
            case SEND_IMG:
                this.imageView.setImageBitmap(BitmapFactory.decodeFile((String) msg.obj));
                break;
        }

    }

    public void setServerMessageView(TextView textView){
        this.serverMessage = textView;
    }
    public void setLc(LaunchConnection lc){
        this.lc = lc;
    }
    public void setImageView(ImageView imageView){ this.imageView =imageView;}
}
