package com.example.sismi.monicaproto2;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

public class PredefinedTextsViewCat2 implements PredefinedViewTexts{

    private int viewSelected=0;
    private Context context;
    private ScrollView scrollView;
    private LinearLayout linearLayout;

    private static String predText1;
    private static String predText2;
    private static String predText3;
    private static String predText4;
    private static String predText5;
    private static String predText6;
    private static String predText7;
    private static String predText8;
    private static String predText9;
    private static String predText10;



    private ArrayList<View> views = new ArrayList<>();

    public PredefinedTextsViewCat2(Context context, LinearLayout linearLayout, ScrollView scrollView) {
        this.context = context;
        predText1 = context.getString(R.string.C2Pred1);
        predText2 = context.getString(R.string.C2Pred2);
        predText3 = context.getString(R.string.C2Pred3);
        predText4 = context.getString(R.string.C2Pred4);
        predText5 = context.getString(R.string.C2Pred5);
        predText6 = context.getString(R.string.C2Pred6);
        predText7 = context.getString(R.string.C2Pred7);
        predText8 = context.getString(R.string.C2Pred8);
        predText9 = context.getString(R.string.C2Pred9);
        predText10 = context.getString(R.string.C2Pred10);

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText1);
        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText2);
        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText3);
        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText4);
        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText5);
        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText6);
        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText7);
        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText8);
        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText9);
        views.add(layoutInflater.inflate(R.layout.bordertxt,null));
        ((TextView)views.get(views.size()-1).findViewById(R.id.predText)).setText(predText10);

        views.get(this.viewSelected).setBackgroundColor(ContextCompat.getColor(context, R.color.colorSelectTextColor)); //Premier selectionné

        this.scrollView = scrollView;
        this.linearLayout = linearLayout;

    }

    public void addViewsTo(){
        for(View view : views){
            linearLayout.addView(view);
        }
    }

    public void removeViewsTo(){
        for(View view : views){
            linearLayout.removeView(view);
        }
    }

    @Override
    public void goDown() {
        if(viewSelected<views.size()-1 ){
            views.get(viewSelected).setBackgroundColor(ContextCompat.getColor(this.context, R.color.colorUnselectedTextBorder)); //previous one
            viewSelected ++;
            views.get(viewSelected).setBackgroundColor(ContextCompat.getColor(this.context, R.color.colorSelectTextColor)); // New one
        }
        scrollView.smoothScrollTo(0,views.get(viewSelected).getTop()- scrollView.getHeight()/2 + views.get(viewSelected).getHeight()/2);
    }

    @Override
    public void goUp() {
        if(viewSelected>0){
            views.get(viewSelected).setBackgroundColor(ContextCompat.getColor(this.context, R.color.colorUnselectedTextBorder)); //previous one
            viewSelected --;
            views.get(viewSelected).setBackgroundColor(ContextCompat.getColor(this.context, R.color.colorSelectTextColor));// New one
            scrollView.smoothScrollTo(0,views.get(viewSelected).getTop()- scrollView.getHeight()/2 + views.get(viewSelected).getHeight()/2);
        }

    }

    public String getSelectedMessage() {
        return ((TextView) views.get(viewSelected).findViewById(R.id.predText)).getText().toString();
    }

    public View getSelectedView(){
        return views.get(viewSelected).findViewById(R.id.predText);
    }
    public Drawable getPredImg(){
        return ((ImageView)views.get(viewSelected).findViewById(R.id.predImg)).getDrawable();
    }
}
