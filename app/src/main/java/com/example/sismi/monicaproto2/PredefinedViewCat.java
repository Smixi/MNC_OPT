package com.example.sismi.monicaproto2;

public interface PredefinedViewCat {
    abstract void addViewsTo();
    abstract void goDown();
    abstract void goUp();
    abstract void removeViewsTo();
    abstract Object getSelectedMessage();
}
