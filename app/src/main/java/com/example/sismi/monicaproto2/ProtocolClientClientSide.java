package com.example.sismi.monicaproto2;

import android.location.Location;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;

public class ProtocolClientClientSide implements ProtocolClient {

	private BufferedOutputStream writer;
	private BufferedInputStream reader;
	private Socket serverToClient;

	public ProtocolClientClientSide(Socket serverToClient) {
		this.serverToClient = serverToClient;
		try {
			this.writer = new BufferedOutputStream(serverToClient.getOutputStream());
			this.reader = new BufferedInputStream(serverToClient.getInputStream());
		} catch (IOException e) {
			System.out.println("Impossible d'avoir le OutPutStream ou le IntputStream vers le Client");
			e.printStackTrace();
		}
	}

	@Override
	public void execute(ProtocolConstants cmd, Object...objects ) {
		//TODO Tout les cas de commande possibles
		System.out.println("Going to do:"+cmd);
		ByteBuffer len = ByteBuffer.allocate(8);
		byte  cmdByte = (byte) cmd.ordinal();

		//Premi�re partie du HEADER
		try {
			this.writer.write(cmdByte);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int sizeBuffer = 8096;


		switch(cmd) {
			case SEND_TXT:

				if(objects.length==0) return; //Le message n'est pas passer en arguments ...
				String str = (String) objects[0];
				System.out.println("Envoi du message: " + str);
				len.putLong((long)str.length()); // On met la taille du string dans len

				try {
					this.writer.write(len.array()); //Partie Len du HEADER
					this.writer.write(str.getBytes());
				} catch (IOException e) {
					System.out.println("Impossible d'envoyer du texte :");
					e.printStackTrace();
				}
				break;
			case CLOSE:
				len.putLong(0);
				try {
					this.writer.write(len.array()); //Partie Len du HEADER
				} catch (IOException e) {
					System.out.println("Impossible de close :");
					e.printStackTrace();
				}
				break;
			case SEND_POS:
				if(objects.length==0) return; //Le message n'est pas passer en arguments ...
				String pos = ((Location) objects[0]).toString();
				System.out.println("Envoi du message: " + pos);
				len.putLong((long)pos.length()); // On met la taille du string dans len

				try {
					this.writer.write(len.array()); //Partie Len du HEADER
					this.writer.write(pos.getBytes());
				} catch (IOException e) {
					System.out.println("Impossible d'envoyer du texte :");
					e.printStackTrace();
				}
				break;
			default:
				System.out.println("Cmd: "+ cmd +" non impl�ment�e");
				break;
		}
		
		try {
			writer.flush();
			Log.d("MSG","CMD "+cmd+"Envoyée");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
