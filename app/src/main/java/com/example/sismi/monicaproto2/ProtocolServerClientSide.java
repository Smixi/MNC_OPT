package com.example.sismi.monicaproto2;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.sql.Timestamp;

public class ProtocolServerClientSide implements ProtocolServer {


	private BufferedOutputStream writer;
	private BufferedInputStream reader;
	private Socket clientToServer;
	private Handler handler = MainActivity.getMainActivityHandler();


	public ProtocolServerClientSide(Socket sock) {
		this.clientToServer = sock;
		try {
			this.writer = new BufferedOutputStream(sock.getOutputStream());
			this.reader = new BufferedInputStream(sock.getInputStream());
		} catch (IOException e) {
			System.out.println("Impossible d'avoir le OutPutStream ou InputStream vers le Client");
			e.printStackTrace();
		}
	}

	@Override
	public void answerTo() throws IOException {

		byte[] header = new byte[9];
		Log.d("READER","Reading: "+clientToServer);
		reader.read(header);
		Log.d("READER","J'ai lu le header");
		ProtocolConstants cmd = getCmd(header[0]);

		BufferedOutputStream bos; //Buffer vers un file
		File f; //Le File


		long len = getLen(header);
		byte[] data = new byte[8096]; //Buffer de transfer
		long count=0; //Compteur d'octets lu
		Message msg;
		int read = 0;
		System.out.println("CMD: "+cmd+", Len: "+len+ ", par "+this.clientToServer);
		//TODO: To be completed to handle each CMD !

		switch(cmd) {
			case SEND_TXT:


				f = MainActivity.getHistoryFile();

				FileWriter fw = new FileWriter(f,true);
				fw.write(new Timestamp(System.currentTimeMillis()).toString()+": ");

				for(;count<len;) {
					read = reader.read(data);
					msg = Message.obtain(handler,cmd.ordinal(),0,0, new String(data));
					msg.sendToTarget();
					fw.write( new String(data));
					count += read;
				}
				fw.write(Misc.newline);
				fw.close();
				break;
			case CLOSE:
				msg = Message.obtain(handler,ProtocolConstants.CLOSE.ordinal());
				msg.sendToTarget();
				break;
			case ERROR:
				throw new IOException("Commande ERROR Reçu");

			case SEND_IMG:
				f = MainActivity.getNewImageFile();
				bos = new BufferedOutputStream(new FileOutputStream(f));
				for(;count<len;) {
					read = reader.read(data);
					bos.write(data,0,read);
					count += read;
				}
				bos.close(); //On ferme le flux, on en a plus besoin.
				msg = Message.obtain(handler,cmd.ordinal(),0,0,f.getAbsolutePath());
				msg.sendToTarget();
				//TODO:Handler a notifié !
				break;
			case SEND_AUDIO:
				f = MainActivity.getNewAudioFile();
				bos = new BufferedOutputStream(new FileOutputStream(f));
				for(;count<len;) {
					read = reader.read(data);
					bos.write(data,0,read);
					count += read;
				}
				bos.close(); //On ferme le flux, on en a plus besoin.
				//TODO:Handler a notifié !
				break;
			default:
				System.out.println("Requete: "+cmd+" pas pris en charge par le serveur");
				break;

		}

	}

	private ProtocolConstants getCmd(byte b) {
		int ord = (int) b;
		if(ord>ProtocolConstants.values().length) {
			throw new IllegalArgumentException("Header pas bon");
			//TODO: Handle errors of this type
		}
		return ProtocolConstants.values()[(int) b];
	}

	private long getLen(byte[] b) {
		return ByteBuffer.allocate(8).wrap(b, 1, 8).getLong();
	}

}
