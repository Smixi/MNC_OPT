package com.example.sismi.monicaproto2;

public interface ProtocolClient {
	public abstract void execute(ProtocolConstants cmd, Object... objects);
}
